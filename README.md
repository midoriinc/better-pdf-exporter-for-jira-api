### What is this repository for? ###

This repository contains the sample post-function and service scripts for the JIRA PDF View Plugin.
These allow automatic generation, saving and emailing PDF documents from JIRA data.

See: http://www.midori-global.com/products/jira-pdf-view-plugin/documentation/api
