import org.apache.log4j.Logger

// JIRA PDF View Plugin
// Groovy script to send a PDF document rendered from a single transitioned issue, attached to an email.
// This is primarily meant to be used as a JIRA workflow post-function with the Script Runner plugin.
// Note: if there are multiple "to" addresses, it generates the PDF file only once and sends the same file to each email address.
// See: http://www.midori-global.com/products/jira-pdf-view-plugin/documentation/api#workflows

log = Logger.getLogger("SendIssuePdfPostFunction.groovy")

evaluate(new File("/path/to/my/post-functions/IntegrationUtils.groovy"))

transitionProperties = transientVars.descriptor.getAction(transientVars.actionId).metaAttributes

// PDF configuration
templateName = integrationUtils.getWorkflowTransitionProperty(transitionProperties, "templateName", "issue-fo.vm", binding.variables)
title = integrationUtils.getWorkflowTransitionProperty(transitionProperties, "title", "${issue.key} export", binding.variables)

// email configuration
to = integrationUtils.getWorkflowTransitionProperty(transitionProperties, "to", "john.doe@acme-company.com", binding.variables) // can contain multiple addresses in a comma-separated list
subject = integrationUtils.getWorkflowTransitionProperty(transitionProperties, "subject", "${title} document", binding.variables)
body = integrationUtils.getWorkflowTransitionProperty(transitionProperties, "body", "Hi!\n\nPlease find the \"${title}\" document in the attachment.\n\n(This email was sent by JIRA PDF View Plugin.)", binding.variables)


pdfResult = integrationUtils.getPdf(templateName, title, issue)
to.tokenize(",").each() {
	integrationUtils.sendFileInEmail(it.trim(), subject, body, pdfResult)
}

log.debug("Post function completed")