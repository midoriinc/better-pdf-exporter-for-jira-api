import org.apache.log4j.Logger
import com.atlassian.jira.component.ComponentAccessor

// JIRA PDF View Plugin
// Groovy script to send a PDF document rendered from a single issue, attached to an email.
// This is primarily meant to be used as a JIRA service with the Script Runner plugin.
// Note: if there are multiple "to" addresses, it generates the PDF file only once and sends the same file to each email address.
// See: http://www.midori-global.com/products/jira-pdf-view-plugin/documentation/api

log = Logger.getLogger("SendIssuePdfService.groovy")

evaluate(new File("/path/to/my/services/IntegrationUtils.groovy"))

// PDF configuration
issueKey = "TEST-1"
templateName = "issue-fo.vm"
title = "${issueKey} export"

// email configuration
to = "john.doe@acme-company.com" // can contain multiple addresses in a comma-separated list
subject = "${title} document"
body = "Hi!\n\nPlease find the \"${title}\" document in the attachment.\n\n(This email was sent by JIRA PDF View Plugin.)"


issue = ComponentAccessor.issueManager.getIssueObject(issueKey)
pdfResult = integrationUtils.getPdf(templateName, title, issue)
to.tokenize(",").each() {
	integrationUtils.sendFileInEmail(it.trim(), subject, body, pdfResult)
}

log.debug("Service completed")