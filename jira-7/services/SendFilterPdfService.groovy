import org.apache.log4j.Logger
import com.atlassian.jira.component.ComponentAccessor

// JIRA PDF View Plugin
// Groovy script to send a PDF document rendered from the results of saved filter, attached to an email.
// This is primarily meant to be used as a JIRA service with the Script Runner plugin.
// Note: if there are multiple "to" addresses, it generates the PDF file only once and sends the same file to each email address.
// See: http://www.midori-global.com/products/jira-pdf-view-plugin/documentation/api

log = Logger.getLogger("SendFilterPdfService.groovy")

evaluate(new File("/path/to/my/services/IntegrationUtils.groovy"))

// PDF configuration
userName = "admin" // this user executes the filter
savedFilterId = 10000
templateName = "issue-fo.vm"
title = "Filter #${savedFilterId} export"

// email configuration
to = "john.doe@acme-company.com" // can contain multiple addresses in a comma-separated list
subject = "${title} document"
body = "Hi!\n\nPlease find the \"${title}\" document in the attachment.\n\n(This email was sent by JIRA PDF View Plugin.)"


user = ComponentAccessor.userManager.getUser(userName)
issues = integrationUtils.runSavedFilter(user, savedFilterId)
pdfResult = integrationUtils.getPdf(templateName, title, issues)
to.tokenize(",").each() {
	integrationUtils.sendFileInEmail(it.trim(), subject, body, pdfResult)
}

log.debug("Service completed")