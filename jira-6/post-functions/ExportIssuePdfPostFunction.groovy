import org.apache.log4j.Logger

// JIRA PDF View Plugin
// Groovy script to export a PDF document rendered from a single transitioned issue, and to copy that to a filesystem path.
// This is primarily meant to be used as a JIRA workflow post-function with the Script Runner plugin.
// See: http://www.midori-global.com/products/jira-pdf-view-plugin/documentation/api#workflows

log = Logger.getLogger("ExportIssuePdfPostFunction.groovy")

evaluate(new File("/path/to/my/post-functions/IntegrationUtils.groovy"))

transitionProperties = transientVars.descriptor.getAction(transientVars.actionId).metaAttributes

// PDF configuration
templateName = integrationUtils.getWorkflowTransitionProperty(transitionProperties, "templateName", "issue-fo.vm", binding.variables)
title = integrationUtils.getWorkflowTransitionProperty(transitionProperties, "title", "${issue.key} export", binding.variables)

// file configuration
// (default path will include a timestamp suffix like "/tmp/TEST-123-20140425-085217.pdf", in order to avoid overwriting a previous export)
path = integrationUtils.getWorkflowTransitionProperty(transitionProperties, "path", "/tmp/${issue.key}-${new Date().format('yyyyMMdd-HHmmss')}.pdf", binding.variables)


pdfResult = integrationUtils.getPdf(templateName, title, issue)
integrationUtils.copyFileToPath(pdfResult, path)

log.debug("Post function completed")